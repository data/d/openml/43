# OpenML dataset: haberman

https://www.openml.org/d/43

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

1. Title: Haberman's Survival Data
 
 2. Sources:
    (a) Donor:   Tjen-Sien Lim (limt@stat.wisc.edu)
    (b) Date:    March 4, 1999
 
 3. Past Usage:
    1. Haberman, S. J. (1976). Generalized Residuals for Log-Linear
       Models, Proceedings of the 9th International Biometrics
       Conference, Boston, pp. 104-122.
    2. Landwehr, J. M., Pregibon, D., and Shoemaker, A. C. (1984),
       Graphical Models for Assessing Logistic Regression Models (with
       discussion), Journal of the American Statistical Association 79:
       61-83.
    3. Lo, W.-D. (1993). Logistic Regression Trees, PhD thesis,
       Department of Statistics, University of Wisconsin, Madison, WI.
 
 4. Relevant Information:
    The dataset contains cases from a study that was conducted between
    1958 and 1970 at the University of Chicago's Billings Hospital on
    the survival of patients who had undergone surgery for breast
    cancer.
 
 5. Number of Instances: 306
 
 6. Number of Attributes: 4 (including the class attribute)
 
 7. Attribute Information:
    1. Age of patient at time of operation (numerical)
    2. Patient's year of operation (year - 1900, numerical)
    3. Number of positive axillary nodes detected (numerical)
    4. Survival status (class attribute)
          1 = the patient survived 5 years or longer
          2 = the patient died within 5 year
 
 8. Missing Attribute Values: None

 Information about the dataset
 CLASSTYPE: nominal
 CLASSINDEX: last

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43) of an [OpenML dataset](https://www.openml.org/d/43). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

